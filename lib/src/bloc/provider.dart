import 'package:apphotel/src/bloc/login_bloc.dart';
import 'package:flutter/material.dart';
// import 'package:apphotel/src/bloc/login_bloc.dart';
export 'package:apphotel/src/bloc/login_bloc.dart';


class Provider extends InheritedWidget {
  

  final loginBloc = LoginBloc();
  
  Provider({ Key? key,  required this.child}) : super(key: key, child: child);

  final Widget child;

  @override
  bool updateShouldNotify(Provider oldWidget) {
    return true;
  }

   static LoginBloc of(BuildContext context) {
    return ( context.dependOnInheritedWidgetOfExactType<Provider>() as Provider ).loginBloc;
  }
}

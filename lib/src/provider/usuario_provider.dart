// import 'package:formvalidation/src/preferencias_usuario/preferencias_usuario.dart';
import 'dart:convert';

// import 'package:dio/dio.dart';
import 'package:apphotel/src/api/api.dart';
// import 'package:http/http.dart' as http;

class UsuarioProvider {

  Future<Map<String,dynamic>> login(String email,String password) async{
    final authData = {
      'email'    : email,
      'password' : password,
      'returnSecureToken' : true
    };    

    final resp = await Network().post(authData, 'user/login');  

    Map<String,dynamic> decodedRes = json.decode(resp.body);  
        
    if (decodedRes.containsKey('token')) {
      print('ok');
      return {'ok':true,'token': decodedRes['token']};
    }else{
      return {'ok': false};
    }
    
  }
}